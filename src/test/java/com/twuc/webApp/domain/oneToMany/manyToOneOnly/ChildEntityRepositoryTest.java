package com.twuc.webApp.domain.oneToMany.manyToOneOnly;

import com.twuc.webApp.domain.ClosureValue;
import com.twuc.webApp.domain.JpaTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

class ChildEntityRepositoryTest extends JpaTestBase {

    @Autowired
    private ChildEntityRepository childEntityRepository;

    @Autowired
    private ParentEntityRepository parentEntityRepository;

    @Test
    void should_save_parent_and_child_entity() {
        // TODO
        //
        // 请书写测试存储一对 one-to-many 的 child-parent 关系。并从 child 方面进行查询来证实存储已经
        // 成功。
        //
        // <--start-
        ClosureValue<Long> closureValue = new ClosureValue<>();
        flushAndClear(em -> {
            ParentEntity parent = new ParentEntity("parent");
            em.persist(parent);

            ChildEntity child = new ChildEntity("child");
            child.setParentEntity(parent);
            em.persist(child);
            closureValue.setValue(child.getId());
        });

        run(em -> {
            ChildEntity childEntity = em.find(ChildEntity.class, closureValue.getValue());
            assertEquals(Long.valueOf(1L), childEntity.getParentEntity().getId());
        });
        // --end-->
    }

    @Test
    void should_remove_the_child_parent_relationship() {
        // TODO
        //
        // 请书写测试：
        //
        // Given 一对 one-to-many 的 child-parent 关系。
        // When 解除 child 和 parent 的关系
        // Then child 和 parent 仍然存在，但是 child 不再引用 parent。
        //
        // <--start-
        ClosureValue<Long> closureValue = new ClosureValue<>();
        flushAndClear(em -> {
            ParentEntity parent = new ParentEntity("parent");
            em.persist(parent);

            ChildEntity child = new ChildEntity("child");
            child.setParentEntity(parent);
            em.persist(child);
            closureValue.setValue(child.getId());
        });

        flushAndClear(em -> {
            ChildEntity childEntity = em.find(ChildEntity.class, 2L);
            childEntity.setParentEntity(null);
        });

        run(em -> {
            ChildEntity childEntity = em.find(ChildEntity.class, 2L);
            assertNull(childEntity.getParentEntity());
        });
        // --end-->
    }

    @Test
    void should_remove_child_and_parent() {
        // TODO
        //
        // 请书写测试：
        //
        // Given 一对 one-to-many 的 child-parent 关系。
        // When 删除 child 和 parent。
        // Then child 和 parent 不再存在。
        //
        // <--start-
        ClosureValue<Long> closureValue = new ClosureValue<>();
        flushAndClear(em -> {
            ParentEntity parent = new ParentEntity("parent");
            em.persist(parent);

            ChildEntity child = new ChildEntity("child");
            child.setParentEntity(parent);
            em.persist(child);
            closureValue.setValue(child.getId());
        });

        flushAndClear(em -> {
            ParentEntity parentEntity = em.find(ParentEntity.class, 1L);
            em.remove(parentEntity);

            ChildEntity childEntity = em.find(ChildEntity.class, 2L);
            em.remove(childEntity);
        });

        flushAndClear(em -> {
            ParentEntity parentEntity = em.find(ParentEntity.class, 1L);
            assertNull(parentEntity);

            ChildEntity childEntity = em.find(ChildEntity.class, 2L);
            assertNull(childEntity);
        });
        // --end-->
    }
}